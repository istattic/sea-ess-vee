# SeaEssVee #
## CSV Import/Export Library For Laravel ##

### Installation ###

Run

```
composer require istattic/sea-ess-vee --save
```

Add

```
ISTAttic\SeaEssVee\Providers\SeaEssVeeServiceProvider::class,
```

To the array of Application Serivce Providers in config/app.php and

```
'SeaEssVee' => ISTAttic\SeaEssVee\Facades\SeaEssVee::class,
```

To the array of Facades

And run 

```
php artisan vendor:publish --provider="ISTAttic\SeaEssVee\Providers\SeaEssVeeServiceProvider"
```