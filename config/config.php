<?php

/*
 * This Is A Part Of ISTAttic\SeaEssVee.
 *
 * (c) 2017 Univerity of Nebraska Omaha IS&T Attic
 * (c) 2014 Sean Tymon
 *
 * View The LICENSE File For Additional Copyright Information.
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Model Name
    |--------------------------------------------------------------------------
    |
    | Set this in your .env file, it will be name of the model you wish to use.
    |
    */

    'model_name' => env('CSV_MODEL_NAME', 'User'),

    /*
    |--------------------------------------------------------------------------
    | Model Mappings
    |--------------------------------------------------------------------------
    |
    | Set this to the fillable fields in the Model you specified above in the
    | 'model_name' field and the more easily readable CSV header text you wish
    | to use.
    |
    */

    'model_mappings' => [
        'Name' => 'name',
        'Email' => 'email',
        'Password' => 'password'
    ]

];
