<?php

/*
 * This Is A Part Of ISTAttic\SeaEssVee.
 *
 * (c) 2017 Univerity of Nebraska Omaha IS&T Attic
 * (c) 2014 Sean Tymon
 *
 * View The LICENSE File For Additional Copyright Information.
 */

namespace ISTAttic\SeaEssVee\Exceptions;

class InvalidFileException extends SeaEssVeeException
{
    public function __construct($code = 0, Exception $previous = null)
    {
        parent::__construct('Invalid file given.', $code, $previous);
    }
}
