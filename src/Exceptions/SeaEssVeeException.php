<?php

/*
 * This Is A Part Of ISTAttic\SeaEssVee.
 *
 * (c) 2017 Univerity of Nebraska Omaha IS&T Attic
 * (c) 2014 Sean Tymon
 *
 * View The LICENSE File For Additional Copyright Information.
 */


namespace ISTAttic\SeaEssVee\Exceptions;

use Exception;

class SeaEssVeeException extends Exception
{
    protected $message = 'An error occured';
}
