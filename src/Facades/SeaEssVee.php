<?php

/*
 * This Is A Part Of ISTAttic\SeaEssVee.
 *
 * (c) 2017 Univerity of Nebraska Omaha IS&T Attic
 * (c) 2014 Sean Tymon
 *
 * View The LICENSE File For Additional Copyright Information.
 */


namespace ISTAttic\SeaEssVee\Facades;

use Illuminate\Support\Facades\Facade;

class SeaEssVee extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'istattic.sea-ess-vee';
    }
}
