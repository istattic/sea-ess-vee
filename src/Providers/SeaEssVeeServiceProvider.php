<?php

/*
 * This Is A Part Of ISTAttic\SeaEssVee.
 *
 * (c) 2017 Univerity of Nebraska Omaha IS&T Attic
 * (c) 2014 Sean Tymon
 *
 * View The LICENSE File For Additional Copyright Information.
 */


namespace ISTAttic\SeaEssVee\Providers;

use Illuminate\Support\ServiceProvider;
use ISTAttic\SeaEssVee\SeaEssVee;

class SeaEssVeeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap Any Application Services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../../config/config.php' => config_path('sea-ess-vee.php'),
        ]);
    }

    /**
     * Register Any Application Services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('istattic.sea-ess-vee', function($app) {
            return new SeaEssVee(
                $this->config('model_name'),
                $this->config('model_mappings')
            );
        });
    }

    /**
     * Helper to get the config values.
     *
     * @param  string  $key
     * @param  string  $default
     *
     * @return mixed
     */
    protected function config($key, $default = null)
    {
        return config("sea-ess-vee.$key", $default);
    }
}
