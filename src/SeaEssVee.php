<?php

/*
 * This Is A Part Of ISTAttic\SeaEssVee.
 *
 * (c) 2017 Univerity of Nebraska Omaha IS&T Attic
 * (c) 2014 Sean Tymon
 *
 * View The LICENSE File For Additional Copyright Information.
 */

namespace ISTAttic\SeaEssVee;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Schema;
use ISTAttic\SeaEssVee\Exceptions\FileNotFoundException;
use ISTAttic\SeaEssVee\Exceptions\InvalidFileException;
use ISTAttic\SeaEssVee\Exceptions\InvalidMappingException;
use ISTAttic\SeaEssVee\Exceptions\InvalidModelException;
use League\Csv\Reader;

class SeaEssVee
{
    protected $model;
    protected $model_mappings;

    public function __construct($model_name, array $model_mappings)
    {
        try
        {
            $model_name = ucfirst($model_name);
            $this->model = app('App\\' . $model_name);
        } catch(Exception $exception) {
            throw new InvalidModelException();
        }

        $this->model_mappings = $model_mappings;
    }

    public function upload(Request $request, $returnData = false)
    {
        if(!$this->areValidMappings())
        {
            throw new InvalidMappingException();
        }

        if(!$request->hasFile('csv_file'))
        {
            throw new FileNotFoundException();
        }

        if(!$request->file('csv_file')->isValid())
        {
            throw new InvalidFileException();
        }

        $path = $request->file('csv_file')->path();
        $csv = Reader::createFromPath($path);
        $headers = $csv->fetchOne();
        $headersLookup = array_flip($headers);

        if(isset($request->replace) && $request->replace == 'yes')
        {
            DB::table($this->model->getTable())->delete();
        }

        $model_count = 0;
        $modelArray = [];

        foreach($csv as $row)
        {
            if($returnData)
            {
                $newModel = [];

                foreach($this->model_mappings as $csv_header => $model_field)
                {
                    $newModel[$model_field] = $row[$headersLookup[$csv_header]];
                }
            } else {
                $newModel = new $this->model;

                foreach($this->model_mappings as $csv_header => $model_field)
                {
                    $newModel->{$model_field} = $row[$headersLookup[$csv_header]];
                }
            }

            if($model_count > 0)
            {
                if($returnData)
                {
                    $modelArray[] = $newModel;
                } else {
                    $newModel->save();
                }
            }

            $model_count += 1;
        }

        try
        {
            Log::info(static::class.': '. "Import completed ", Request::capture()->ips());
        } catch(Exception $exception) {
            throw new InvalidModelException();
        }

        if($returnData)
        {
            return $modelArray;
        }

        if(!$returnData)
        {
            return ($model_count - 1);
        } else {
            throw new InvalidMappingException();
        }
    }

    public function export($filename, $template = false)
    {
        if(!$this->areValidMappings())
        {
            throw new InvalidMappingException();
        }

        try
        {
            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename="' . $filename . '.csv"');
            header('Cache-Control: no-cache, no-store, must-revalidate');
            header('Pragma: no-cache');
            header('Expires: 0');

            $file = fopen('php://output', 'w');

            fputcsv($file, array_keys($this->model_mappings));

            if(!$template)
            {
                $csv_row = [];
                
                foreach(DB::table($this->model->getTable())->get() as $model)
                {
                    $counter = 0;

                    foreach ($this->model_mappings as $csv_header => $model_field)
                    {
                        $csv_row[$counter] = $model->{$model_field};
                        $counter += 1;
                    }

                    fputcsv($file, $csv_row);

                    try
                    {
                        Log::info(static::class . ': ' . "Model export completed ", ["model id:" . $this->model->id, Request::capture()->ips()]);
                    } catch (Exception $exception) {
                        throw new InvalidModelException();
                    }
                }
            }

            fclose($file);
        } catch(Exception $exception) {
            throw new InvalidFileException();
        }
    }

    protected function areValidMappings()
    {
        try
        {
            foreach($this->model_mappings as $csv_header => $model_field)
            {
                if(!Schema::hasColumn($this->model->getTable(), $model_field))
                {
                    return false;
                }
            }
        } catch(Exception $exception) {
            return false;
        }

        return true;
    }
}
